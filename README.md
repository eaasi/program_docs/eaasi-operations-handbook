# EaaSI Operations Handbook

The [EaaSI Operations Handbook](https://eaasi.gitlab.io/program_docs/eaasi-operations-handbook/index.html) is the primary source for documentation on policies and procedures that inform how the program . As part of our commitment to openness and transparency, we are making these documents public even as we continue to adjust and improve our procedures.

For documentation related to the setup and use of the EaaSI system, please consult the [EaaSI User Handbook](https://eaasi.gitlab.io/eaasi_user_handbook/index.html).