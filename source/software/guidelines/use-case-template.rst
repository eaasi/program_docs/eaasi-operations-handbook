.. Use Case Template

.. _use-case-template:

Use Case Template
#################

Description
===========

Summarize the use case in 1-2 sentences. 

Pre-Condition
=============

Document any preceeding steps or system settings that must be complete(d) before the use case can begin.

Ordinary Sequence
=================

In numbered order, list the steps that the user and system, in response, must take to complete the use case.

Post-Condition
==============

Document the desired state at the end of the workflow.

Error or Alternate Sequence
===========================

Identify the starting point from the ordinary sequence and document in numbered order any alternate paths or concluding errors that may present themselves as part of the workflow.

Example
=======

    **Description:** A system user copies the access link to an environment and sends it to an access user. The access user logs in and interacts with the environment.

    **Pre-Condition:** Environment created in node; End user access settings configured (e.g., access link turned on, concurrent sessions, length of session, etc.) Access user account created

    **Ordinary Sequence:**

    1. The *user* clicks "Get Shareable Link" (or something similar)
    2. The *system* indicates that link is copied to comp's clipboard
    3. The *user* sends the link to the *access user*
    4. The *access user* clicks the link
    5. The *system* requests login credentials from the access user
    6. The *access user* enters their username and password
    7. The *system* authenticates the user's credentials
    8. The *system* verifies that the access link is still active and there are still seats avail according to concurrent session settings
    9. The *system* starts up the environment
    10. The *access user* interacts with the environment
    11. The *access user* closes the window and ends their session

    **Post-Condition:** Access user's session is complete

    **Error Sequence - Access Link Expired**

    9. The *system* alerts the user that the environment is no longer accessible from the link they are using

    **Error Sequence - Concurrent Session Limit**

    9. The *system* alerts the access user that the environment is currently in use and they should try again later

    **Alternate Sequence - Session Extended**

    11. The *access user* reaches the end of their session time limit
    12. The *system* indicates that their session will end unless they extend
    13. The *access user* extends their session

    **Alternate Sequence - Session Times Out**
    
    11. The *access user* reaches the end of their session time limit
    12. The *system* indicates that their session will end unless they extend
    13. The *access user* doesn't respond
    14. The *system* ends the user's session
