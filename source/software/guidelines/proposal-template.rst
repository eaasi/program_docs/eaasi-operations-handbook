.. Proposal Template

.. _proposal-template:

Proposal Template
#################

User Story
==========

*Describe the basic function and outcome of the feature in standard user story form: As a [type of user], I need [proposed function], so that [desired outcome]*

Edition
=======

*Mark which edition of the system should include the new feature below:*

[ ] Local
[ ] Hosted
[ ] Both


Requirements
============

*List any known requirements of the proposed feature.*

Use Cases
=========

*Attach files of any existing use cases describing proposed workflows.*

Proposed Iterations
===================

*If relevant, list possible iterations of the feature that could be developed for subsequent releases.*
