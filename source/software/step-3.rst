.. Step 3 – Documentation & Release

.. _documentation-release:

Step 3 – Documentation & Release
################################

Prior to release, whether via automated deployment to the hosted edition or as an update to local editions, software updates must have comprehensive documentation outlining underlying concepts, guidelines for use, and tutorials for configuration of any technical elements (e.g., APIs, sys admin settings, etc.). Only once documentation is complete will new functionality or fixes be considered ready for release. 

Releases are timed according to the program timeline and decisions of the strategic planning group. Leading up to the release date, EaaSI team members will undertake necessary communications to alert local edition users of the available update and hosted edition users of any downtime required for update.

  * :ref:`handbook-update`
  * :ref:`release-management`

.. _handbook-update:

Step 3.1 EaaSI Handbook Update
==============================

**Participants:**

* User Support Lead

**Outcome** New workflows and changes incorporated into EaaSI documentation

**Process** 

**1.** Should a new feature or update to the system significantly alter a core workflow of EaaSI, an update to the `EaaSI User Handbook <https://eaasi.gitlab.io/eaasi_user_handbook/index.html>`_ is required. Upon successful completion of testing, the User Support Lead, in conjunction with the Program Manager and Lead Developer, determines a release deadline for the new feature.

**2.** The User Support Lead identifies and makes necessary changes to the `EaaSI User Handbook <https://eaasi.gitlab.io/eaasi_user_handbook/index.html>`_ at least *one week* prior to the release date. Changes are not merged into the handbook repository until the release is live.

.. _release-management:

Step 3.2 Release Management
==============================

**Participants:**

* Program Manager
* User Support Lead
* Chief Programmer

**Outcome** Maintenance window scheduled, release notification posted to EaaSI forum, and updated code implemented in hosted service or made available for update to self-deployed nodes.

**Process**

**1.** Having set a deadline for the completion of new code and updated the `EaaSI User Handbook <https://eaasi.gitlab.io/eaasi_user_handbook/index.html>`_, new features are now considered ready for release. According to the deadline set after testing, the User and Technical Support Group determines an appropriate date and time to implement the code changes and make available the updated code for self-deployed nodes. This maintenance window is communicated to the EaaSI team at least *two weeks* prior.

**2.** *One week* before the scheduled maintenance window, a notification of release is published in the `Project Announcements <https://forum.eaasi.cloud/c/project-announcements/5>`_ channel of the `EaaSI Community Forum <https://forum.eaasi.cloud/>`_. The announcement contains release notes detailing the new features and updates made to the system. The estimated down time for the cloud hosted edition of EaaSI is also communicated in this notice.

**3.** During the scheduled maintenance window, the Chief Programmer implements changes to the EaaSI code base and conducts quality assurance checks on the cloud-hosted edition. If an update breaks the cloud-hosted edition or causes any issues with existing or new functionality, the Chief Programmer will attempt to troubleshoot during the scheduled maintenance window. Should a fix require more time than scheduled, a notice will be posted in the `Project Announcements <https://forum.eaasi.cloud/c/project-announcements/5>`_ channel of the forum. 

Upon successful implementation of the updates, the User Support Lead merges changes to the `EaaSI User Handbook <https://eaasi.gitlab.io/eaasi_user_handbook/index.html>`_ to bring them up-to-date with the current state of the system.

