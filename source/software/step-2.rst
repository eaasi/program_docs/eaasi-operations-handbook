.. Step 2 – Implementation & Testing

.. _implementation-testing:

Step 2 – Implementation & Testing
#################################

The second phase of the process encompasses the coding, testing, building/staging, and verification of new functionality prior to release. If changes to the software meet the acceptance criteria defined during the planning phase, they proceed to documentation and release. 

  * Step 2.1: Coding
  * Step 2.2: Build and Automated Tests
  * Step 2.3: Refinement and Stabilization
  * Step 2.4: Code Review
  * Step 2.5: Staging and Acceptance Testing
