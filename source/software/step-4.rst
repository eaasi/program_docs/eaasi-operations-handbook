.. Step 4 – Maintenance & Improvement

.. _maintenance-improvement:

Step 4 – Maintenance & Improvement
##################################

The EaaSI program is committed to ongoing improvement and optimization of the system and does not expect any new product to be perfect upon release. Following release, the team will monitor the response to new functionality or other changes and determine whether further iterations, bug fixes, or improvements are warranted. The product development cycle then repeats, as any potential development requiring substantial change to the code base, architecture, or metadata model is submitted as a proposal returning the process to step one, specification and design.
