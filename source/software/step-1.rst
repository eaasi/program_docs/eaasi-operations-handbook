.. Step 1 – Plan & Design

.. _plan-design:

Step 1 – Plan & Design
######################

Product development starts with initial planning during which a feature or improvement is proposed; requirements and concepts are defined; and a development approach and work packages are specified. Proposals address conceptual details of new features, improvements, or planned maintenance that may require substantial changes to the EaaSI code base, system architecture, or metadata model (as opposed to a minor bug fix or other correction). 

Depending on the size, complexity, and urgency of the proposed work, features or improvements may be reviewed by the EaaSI Strategic Planning Group, who will either accept the proposal (thus adding it to a release schedule and proceeding to the next phase), decline it, or postpone for future consideration. Accepted proposals are assigned to an upcoming release and added to the program timeline for monitoring through subsequent phases.

  * :ref:`proposal-submission`
  * :ref:`proposal-review`
  * :ref:`ux-ui`
  * :ref:`implementation-planning`

.. _proposal-submission:

Step 1.1 Proposal Submission
============================

**Participants:**

* EaaSI Team members
* EaaSI Community members

**Guidelines:**

* :ref:`proposal-template`
* :ref:`use-case-template`

**Outcome:** Proposal issue created in ``eaasi_project``

**Process**

Submit proposals :ref:`submit-gitlab` or :ref:`submit-forum` according to the instructions below.

.. _submit-gitlab:

via GitLab
**********

**1.** In the ``eaasi_project`` repository, create a new issue for your feature proposal. 

.. image:: ./images/1-1_Figure1.png
	   :align: center
	
**2.** Name your feature proposal with a short description of the functionality or workflow.

.. image:: ./images/1-1_Figure2.png
	   :align: center

**3.** Select the ``feature-proposal`` template from the Description dropdown

.. image:: ./images/1-1_Figure3.png
	:align: center
	
**4.** Fill out the feature-proposal information as thoroughly as possible. Attach supplemental documentation (metadata specifications, use cases, design mockups, etc.) as files or wait to add them to comments after creating the issue.

**5.** Once all documentation is complete, click ``Create Issue.``

.. image:: ./images/1-1_Figure4.png
	:align: center


									**Way to go! Your proposal is submitted!**

.. _submit-forum:

via EaaSI Forum
***************

**1.** Create a New Topic in the Support Center category with the title *Feature Proposal - [Short Title]*. Add the tag “new feature” or “improvement.”

.. image:: ./images/1-1_Figure5.png
	:align: center

**2.** In the body of the post, add the text of your proposal. Guidelines for the structure and contents of proposals are available in the :ref:`proposal-template`.

.. image:: ./images/1-1_Figure6.png
	:align: center
	
**3.** Add supplemental documentation as an attachment to your post.

		*Please include as much supplemental documentation as possible, such as: specification of new or changed metadata properties, use cases (see :ref:`use-case-template`) design mockups, etc.*

**4.** Once all documentation is complete and added to the post, click “Create Topic” to complete your proposal.

**Way to go! Your proposal is submitted!**
  
.. _proposal-review:

Step 1.2 Proposal Review
========================

**Participants:**

* EaaSI Team members
* EaaSI Community members

**Outcome:** Proposed feature is scoped and prioritized

**Process**

**1.** Once a feature proposal is submitted to the ``eaasi_project`` repository, the EaaSI Program Manager conducts an initial review and assigns appropriate labels. The issue is also be logged in the team's project management system for tracking through the review, design, and implementation processes.

.. image:: ./images/1-2_Figure1.png
	:align: center

**2.** Team members are notified of the proposed feature via Slack and provided a month for review. 

Questions regarding the proposal specification and clarification of its various elements will be posted to the comments section of the issue in GitLab. 

		*The proposer must reply in a timely manner to enable an active conversation about the feature and its impact on the system and its users. Team members will do their best to answer any unanswered questions, but a lack of engagement may impact a feature's prioritization.*

Should the feature be proposed in the EaaSI Community Forum, the Program Manager monitors the post for comments and questions from the community. Relevant information is transferred to the comments section of the GitLab issue.

**3.** Upon completion of the review period, the Strategic Planning Group decides on a prioritization level for the feature. EaaSI uses the MoSCoW method of prioritization, assigning one of the following values:

-  **M** ust – The feature is critical to the successful completion of the current development cycle or program timeline. 
-  **S** hould – The feature is important but not mandatory for delivery in the current development cycle or program timeline.
-  **C** ould – The feature is nice to have and will be implemented if time and staff capacity permit.
-  **W** on't - The feature may have value but is not critical to the system or program and will not be pursued at this time.

**Way to go! Your proposal is now complete!**

.. _ux-ui:

Step 1.3 UX/UI Design
=====================

**Participants:**

* Program Manager
* UX/UI Designer
* EaaSI Team members

**Outcome:** Feature workflows are defined and interface designs completed

**Process**

**1.** Once scoping and prioritization are complete, a feature proposal enters the queue for UX/UI design. The order in which features are designed is determined by the Strategic Planning Group's management of the EaaSI development roadmap.

**2.** To begin the UX/UI design process, the Program Manager and Designer review the submitted documentation, determine how the feature fits within existing or new workflows, and identify the necessary interface components to design.

**3.** A first draft of the design is completed and provided to the EaaSI team for review. Team members have two weeks to provide feedback.

**4.** Design and review continue until consensus is reached by the team.

.. _implementation-planning:

Step 1.4 Implementation Planning
================================

**Participants:**

* Strategic Planning Group
* Dev Planning Group

**Outcome:** Approach for development, implementation, and release target complete

**Process**

**1.** Upon completion of UX/UI design, a feature enters the development backlog. A new feature is again prioritized within the development backlog by the Strategic Planning Group according to the current needs of the program and stakeholders.

**2.** A feature scheduled for development is first reviewed by developers to determine how related functionality will be implemented in the back end and to determine the scale of the development effort.

**3.** A scoped feature is again reviewed by the Strategic Planning Group to determine whether it is ready for development and to schedule it for development. If the Strategic Planning Group determines that a feature is not prepared for development it may return to a previous step in the Plan & Design process until all outstanding issues are corrected.
