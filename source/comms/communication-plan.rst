.. Communication Plan

EaaSI Communication Plan
########################

.. _comms-objectives:

Communication Objectives
========================

The purpose of this communications plan is to ensure that the EaaSI program is effectively sharing and translating the work of the program staff, as well as actively engaging new and existing stakeholders that are critical to the success of EaaSI.

Primarily, the EaaSI program team will communicate the immediate relevance of EaaSI and its complementary businesses/services through “emulation in action” messaging, which frames EaaSI functionality within an illustrative use case such as:

* Reproducibility, reusability, and data management
* Virtual reading room for archives and special collections

  * Architectural and design records

* EaaSI in the classroom as a pedagogical tool
* Using emulation in digital preservation workflows

Program communication objectives advance the program deliverables outlined in the :ref:`program-roadmap`, helping to generate additional support for our efforts and encourage participation from the community of stakeholders and potential users. 

+--------------------------------------------------+-------------------------------------------------------------+
| Objectives (Internal and External Stakeholders)  | Measure                                                     |
+==================================================+=============================================================+
| Secure the commitment of a defined               | Additional organizational commitments to the EaaSI service. |
| group of stakeholders to the program aims        |                                                             |
+------------------------+------------+------------+-------------------------------------------------------------+
| Demonstrate relevance and utility of the EaaSI   | Track engagement with specific user communities and         |
| service to specific user communities and         | associated use cases including those listed above.          |
| associated use cases.                            |                                                             |
+--------------------------------------------------+-------------------------------------------------------------+
| Clarify relationships between key stakeholder    | Track how new users and partners are finding their way to   |
| groups and EaaSI.                                | the program staff.                                          |
+--------------------------------------------------+-------------------------------------------------------------+
| Develop a clear, branded set of “onboarding”     | Use of this set of materials and ability to operationalize  |
| activities or “getting started” curriculum that  | the data set resulting from use of these materials.         |
| both builds capacity of EaaSI members and/or     |                                                             |
| potential members AND helps to develop a         |                                                             |
| longitudinal data set that marks key moments in  |                                                             |
| the ongoing development of organizations software|                                                             |
| curation program.                                |                                                             |
+--------------------------------------------------+-------------------------------------------------------------+
| Transparency regarding project sponsorship,      | Track how many questions regarding underlying rationales    |
| identified stakeholders, driving rationales for  | and data are asked of the team on a regular basis - show    |
| the selection of key stakeholders, data          | evidence of a meaningful shift from one-on-one queries to   |
| gathering methods that influence the development | staff to increased use of the knowledge base.               |
| of the service and prioritization decisions.     |                                                             |
+--------------------------------------------------+-------------------------------------------------------------+

.. _target-audiences:

Target Audiences
================

**Primary** target audiences include:

* Project Sponsors
* Yale internal stakeholders and users
* EaaSI Nodes
* SPN Members
* Stakeholder groups that correspond directly to the use cases/pilots that will be deployed as part of the project: 

  * Scientific software community - science and engineering researchers
  * Consortia and organizations that may want to deploy an emulation and share software images within the network 
  * Archives that provide reading room access to collection materials

* Potential participants in the EaaSI Network or users of EaaSI Services:

  * Software creators
  * Major digital preservation, digital scholarship, open data and open access advocacy groups with large membership or broad audience:

    * SPARC
    * CNI
    * LPC
    * DPC
    * Internet Archive

  * Groups representing the scientific software community
  * Research reproducibility groups
  * Architectural records groups
  * Digital pedagogy groups
  * Publishers of complex/multi modal publications

    * ACM
    * IEEE
    * University Presses (Stanford)

  * Digital Preservation Consortia:

    * APTrust
    * HathiTrust
    * MetaArchive
    * ScholarsPortal
    * COPPUL
    * BitCurator Consortium
    * Nestor

  * Metadata:

    * Wikidata
    * CodeMeta

**Secondary** target audiences include:

* Related projects:

  * Projects addressing the issue of technological obsolescence and long term access to software-dependent objects through training & education
  * Projects addressing the issue of technological obsolescence and long term access to software-dependent objects through platform development
  * Projects addressing the issue of technological obsolescence and long term access to software-dependent objects through description and collection development

.. _editorial-campaigns:

Editorial Campaigns
===================

Key Messages
++++++++++++

**EAASI COMMUNITY ENGAGEMENT**

Meaningful community engagement is not just nice to have, it is an essential and mutually beneficial strategic function that results in better-informed and more effective policies, projects, programs and services. Evidence and experience both demonstrate that the earlier stakeholders are engaged, the more likely benefits will be realized. 

For stakeholders, the benefits of engagement include the opportunity to contribute to policy and program development, have their issues heard and participate in the decision-making process. 

For EaaSI, the benefits of stakeholder engagement include:
 
1. Improved communication between EaaSI and other individuals, groups and organizations that are members of the software preservation and emulation communities of practice
2. Access to critical information about peer organizations and their experiences from researching local user needs, testing EaaSI workflows and direct influence in shaping/prioritizing features of the EaaSI service
3. Support for pilot system implementation and workflow testing by members of the project team at no cost to the stakeholder organization

**DEFINING THE EAASI COMMUNITY**

*EaaSI Network Members*: Organizations actively engaged in the use of the EaaSI system, whether for testing/capacity building purposes or to support services at their institution.

* The Network is an opt-in membership model, members agree to use the service and participate in the program according to terms specified in the EaaSI Participation Agreement, including:

  * Contribution to the development of technology and services through software testing, evaluation of program plans and system features, implementation of the system within their own institution, and creation of public-facing resources for the broader software preservation community

* Members attend in monthly cohort meetings to provide updates on their use of the EaaSI system and the progress of the program
* Members may belong to any recognized network of EaaSI nodes (e.g., North American network, Australasia network)

  * Resources may be shared for the mutual benefit and reuse by all members of their network
  * By publishing a resource, members grant EaaSI and its users the legal right to share and host the resource in perpetuity

Campaigns
+++++++++

EaaSI Resource Highlights

  * Details:

    * Directing the attention of specific communities (corresponding to emulation use cases) to specific resources on the website and providing some context on why it is relevant to their community
    * Work needs to be done here to make sure that all our past work is actually on line - there is a subset of materials that have been published but that doesn’t capture the full gamut of activity from Phase 1

  * Channels: (website, tweet, listserv, osf) 
  * Frequency: Monday of the 3rd week of the month

Training Modules + Demo videos

  * Details: 

    * In-depth explanations of EaaSI’s features - a distinct way of indexing in to EaaSI, in contrast to demonstrated, bundling functionality or features under a use case, this approach is closer to the “Swiss Army Knife” analog - taking specific features and showing how they work and could be applied to NUMEROUS use cases

    * Includes: video, slides, list of related resources (to go even further)

  * Channels: (website, tweet, listserv, Youtube) 
  * Frequency: Every three months, the first week of the month starting October 7, 2020

Case Studies

  * Details: 

    * Deep dive on an environment that features a specific use case - we can create our own iteration of Amelia’s use case template that she created for the FCoP environment deep dives

    * Bringing together deep dives, and training modules together in one thing

  * Channels: (website, tweet, listserv) 
  * Frequency: Every six months, Wednesday of the second FULL week starting September 16, 2020

Project updates

  * Details:

    * Student staff configuration updates - people LOVE these highlights
    * Recap of events staff has attended or demonstrations to new partners and communities
    
  * Channels: (website, STACKTRACE) 
  * Frequency: Every two months, the first full week of the month, starting August 4, 2020

.. _events-conferences:

Events & Conferences
====================

Tracking Conferences
++++++++++++++++++++

The project staff represents distinct, although sometimes overlapping communities of practice. The project team is also accountable to funders and future/potential stakeholders to approach conference attendance strategically. `EaaSI Conference Guidelines <https://docs.google.com/document/d/1ayga_98kQaGzdkH3rzBR1EvtaL6RJFpcTGhgUWiFj2I/edit?usp=sharing>`_

1. Copy and paste the link to the CfP in the #conferences slack channel with an @jmeyerson notification so that the upcoming CfP can be added to the `EaaSI Conference Tracker <https://docs.google.com/spreadsheets/d/1Sliu1flKDHzSf1u4895Wr4etXZN8sfJ5YWfTdtBIqSo/edit?usp=sharing>`_
2. Proposal drafts, submitted proposals, slide decks and speaker notes (where applicable) should be created in or uploaded to the `Conferences and Presentations <https://drive.google.com/drive/folders/0BwT-Vd0f3UoLVy1QZHo2aU0tejA?usp=sharing>`_ subfolder in the appropriate year within the EaaSI Google Drive Folder.
3. All conference presentations about the EaaSI project should conform to the brand styleguide that will be made available in the EaaSI Google Drive Folder.
