.. EaaSI docs

EaaSI Operations Handbook
########################
The EaaSI Operations Handbook is the primary source for documentation on policies and procedures that inform how the program . As part of our commitment to openness and transparency, we are making these documents public even as we continue to adjust and improve our procedures.

For documentation related to the setup and use of the EaaSI system, please consult the `EaaSI User Handbook <https://eaasi.gitlab.io/eaasi_user_handbook/index.html>`_.

.. toctree::
   :maxdepth: 1
   :caption: Program Charter

   charter/problem-statement.rst
   charter/what-is-eaasi.rst
   charter/values.rst
   charter/roadmap.rst
   
.. toctree::   
   :maxdepth: 1
   :caption: EaaSI Team Operations
   
   team/members.rst
   team/groups.rst
   team/strategic-planning.rst
   team/change-mgmt.rst
   
.. toctree::
   :maxdepth: 2
   :caption: Development and Maintenance
   
   software/step-1.rst
   software/step-2.rst
   software/step-3.rst
   software/step-4.rst

.. toctree::   
   :maxdepth: 2
   :caption: Community Development
   
   comms/communication-plan.rst
