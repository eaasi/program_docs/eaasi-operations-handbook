.. Problem Statement

Problem Statement
#################

Software is everywhere. The digital information that drives research, culture, industry (healthcare, utilities, transportation, etc.), and many facets of daily life depends on it; a dependency that is challenged by the complexity and inevitable obsolescence of technology. To ensure the longevity of essential services and the record of this digital heritage, many organizations are engaged in the collection and preservation of software. But, acquisition alone does not guarantee software will remain usable, now or in the future. Preserved software and the information that uses it may remain inaccessible despite our best efforts.

Emulation of computing environments is recognized as a strategy for this problem of software preservation and access, but the deployment of emulators requires specialist expertise and financial investment in infrastructure. Emulation-as-a-Service (EaaS), developed by a team at the University of Freiburg and maintained by OpenSLX, established a technological framework to simplify the configuration and operation of emulators for use of preserved software and access to digital information. While the technology may lower the barrier of entry to emulation, investment in EaaS remains hindered by various factors, including resource requirements for acquisition of software, developing communities of practice for emulation and software preservation, and an overall lack of understanding or knowledge of the use cases for emulation. 

Emulation of software is fundamental to the access and reuse of digital systems, services, and heritage. Immediate action is required to establish sufficient technology and services to provide emulation at scale.
