.. Program Roadmap

.. _program-roadmap:

Program Roadmap
###############

Objectives
==========
* Establish a global network of EaaSI nodes, or separate networks, exchanging software and configured environments via the EaaSI service
* Integrate EaaSI services in digital access platforms as the default mechanism for delivering digital collections to students, faculty, and researchers
* Institute the EaaSI metadata framework as a standard for description of software and computing environments
* Sustain operation of the EaaSI program to enable research and development, support and maintenance, and other activities in service to our users
* Support digital content access via EaaSI for tens of thousands of organizations of different types, sizes, and capacity
* Collaborate with publishers and developers to establish a long-term home for their software
* Eliminate the prospect of obsolescence for any and all types of software, computer systems, and digital materials from the past and future

Timeline
========
Phase 1 | January 2018 – June 2020
----------------------------------
Funded by the Alfred P. Sloan Foundation and the Andrew W. Mellon Foundation

Goal
^^^^
Deploy and scale infrastructure and services for software emulation, including distributed management, documentation, sharing, discovery, and access.

Deliverables
^^^^^^^^^^^^

+---------------------------------------------------------------------+----------------------------------------------------------------------------------------------+
|**Deliverable**                                                      |**Outcome**                                                                                   |
+=====================================================================+==============================================================================================+
|**Deployed network of EaaSI nodes in which software is shared**      |EaaSI nodes deployed at six partner institutions                                              |
+---------------------------------------------------------------------+----------------------------------------------------------------------------------------------+
|**Metadata application profile for EaaSI**                           |Specification of EaaSI metadata model complete and prepared for implementation                |
+---------------------------------------------------------------------+----------------------------------------------------------------------------------------------+
|**Shared collection of processed software**                          |2,000 emulation environments prepared for access and/or re-use by EaaSI nodes and researchers |
+---------------------------------------------------------------------+----------------------------------------------------------------------------------------------+
|**Authentication service implemented in EaaSI system**               |Prototype authentication service implemented for updated EaaSI interface                      |
+---------------------------------------------------------------------+----------------------------------------------------------------------------------------------+
|**Completed Universal Virtual Interactor (UVI) API**                 |Prototype of UVI automation service completed and prepared for implementation in system       |
+---------------------------------------------------------------------+----------------------------------------------------------------------------------------------+
|**Portal dedicated to Scientific Software Preservation and Access**  |Analogous functionality designed and prepared for implementation                              |
+---------------------------------------------------------------------+----------------------------------------------------------------------------------------------+
|**Portal dedicated to sharing CD-ROM publications**                  |Service and software infrastructure designed and pending implementation                       |
+---------------------------------------------------------------------+----------------------------------------------------------------------------------------------+
|**Tool for management of restricted access in virtual reading room** | Functionality designed and pending implementation                                            |
+---------------------------------------------------------------------+----------------------------------------------------------------------------------------------+

Milestones
^^^^^^^^^^

+------------------+----------------------------------------------------------------------------------+
|**May 2018**      |Acquisition of copy of NSRL software collection                                   |
+------------------+----------------------------------------------------------------------------------+
|**June 2018**     |Five institutions begin participation as initial nodes in EaaSI Network           |
+------------------+----------------------------------------------------------------------------------+
|**November 2018** |EaaSI Network kick-off meeting                                                    |
+------------------+----------------------------------------------------------------------------------+
|**March 2019**    |EaaSI beta software released with network sharing functionality                   |
+------------------+----------------------------------------------------------------------------------+
|**July 2019**     |EaaSI beta update released to nodes                                               |
+------------------+----------------------------------------------------------------------------------+
|**August 2019**   |Multithreading Software Preservation and Emulation workshop at 2019 SAA Conference|
+------------------+----------------------------------------------------------------------------------+
|**September 2019**|UVI prototype completed and presented at iPres conference                         |
+------------------+----------------------------------------------------------------------------------+
|**February 2020** |Georgia Tech joins EaaSI Network                                                  |
+------------------+----------------------------------------------------------------------------------+
|**March 2020**    |EaaSI v2020.03-beta released with new user interface                              |
+------------------+----------------------------------------------------------------------------------+

Phase 2 | July 2020 – June 2022
-------------------------------
Funded by the Alfred P. Sloan Foundation and the Andrew W. Mellon Foundation

Goal
^^^^
Develop a stable and maintainable version of the EaaSI software and related support services that will attract new users and encourage sustainable investment in the program.

Deliverables
^^^^^^^^^^^^

+---------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------+
|**Deliverable**                                                            |**Outcome**                                                                                                  |
+===========================================================================+=============================================================================================================+
|**Updates to the EaaSI code base**                                         | * Three releases of updated software, including:                                                            |
|                                                                           |                                                                                                             |
|                                                                           |   * Mobile emulation                                                                                        |
|                                                                           |   * Networked Environments                                                                                  |
|                                                                           |   * Interaction API                                                                                         |
|                                                                           |   * Incorporation of UVI functionality                                                                      |
|                                                                           |   * Photographs and screenshot display                                                                      |
|                                                                           |   * Ranking/voting of resource quality                                                                      |
|                                                                           |                                                                                                             |
|                                                                           |* Target Level AA conformance with the World Wide Web Consortium’s (W3C) Web Content Accessibility Guidelines|
+---------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------+
|**Maintenance of existing infrastructure**                                 |* Completion of code/architecture review                                                                     |
|                                                                           |* Documentation of all system components                                                                     |
|                                                                           |* Definition of programming style guide and open source contribution policy                                  |
|                                                                           |* Implementation of maintenance policy and publication of quarterly maintenance reports                      |
|                                                                           |* Ongoing response to bug reports and troubleshooting                                                        |
+---------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------+
|**Expansion of the EaaSI stakeholder community and user base**             |* Outreach strategies for at least 5 target stakeholder groups implemented                                   |
|                                                                           |* At least 10 new institutions hosting EaaSI nodes                                                           |
|                                                                           |* Completion of a hosted service pilot program with members of the Software Preservation Network             |
|                                                                           |* Aggregate end-user counts in the 100,000’s                                                                 |
|                                                                           |* 1 new resource published to the EaaSI website each month                                                   |
|                                                                           |* 2 EaaSI workshop events produced each year                                                                 |
|                                                                           |* Publication of research papers in at least 2 journals                                                      |
+---------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------+
|**Improvement of user support services and engagement activities**         |* Support requests addressed and closed within two weeks of submission on average                            |
|                                                                           |* One activity completed by an EaaSI partner each quarter                                                    |
|                                                                           |* At least 1,000 new environments and software records published to EaaSI networks                           |
|                                                                           |                                                                                                             |
|                                                                           |  * 15 contributions of open source software environments from the Public Sandbox to EaaSI network           |
+---------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------+
|**Development of organizational infrastructure and governance frameworks** |* Governance plan for EaaSI Network and services                                                             |
|                                                                           |* EaaSI Service Prospectus and Sustainability Plan                                                           |
+---------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------+

Milestones
^^^^^^^^^^

+-------------------+------------------------------------------------------------------------------------------------------+
| **December 2020** |Digital Publications Service prototype complete, Emulation-in-action Case Study #1 published          |
+-------------------+------------------------------------------------------------------------------------------------------+
| **January 2021**  |EaaSI v2021.01 release and hosted service launch                                                      |
+-------------------+------------------------------------------------------------------------------------------------------+
| **February 2021** |SPN hosted service pilot begins, EaaSI user forum launch                                              |
+-------------------+------------------------------------------------------------------------------------------------------+
| **June 2021**     |EaaSI v2021.06 release, Emulation-in-action Case Study #2 published                                   |
+-------------------+------------------------------------------------------------------------------------------------------+
| **September 2021**|SPN hosted service pilot ends                                                                         |
+-------------------+------------------------------------------------------------------------------------------------------+
| **December 2021** |EaaSI service prospectus complete, EaaSI v2021.12 release, Emulation-in-action Case Study #3 published|
+-------------------+------------------------------------------------------------------------------------------------------+
| **January 2022**  |Public EaaSI node pilot begins, Fundraising and membership campaign begins                            |
+-------------------+------------------------------------------------------------------------------------------------------+
| **June 2022**     |EaaSI v2022.06 release, Emulation-in-action Case Study #4 published                                   |
+-------------------+------------------------------------------------------------------------------------------------------+
