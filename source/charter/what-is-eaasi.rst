.. What is EaaSI?

.. _what-is-eaasi:

What Is EaaSI?
##############

*Usable software, forever*

EaaSI provides technology and services for the application of software emulation across a diverse spectrum of professional disciplines, organizations, and individual use cases. With an innovative software solution and a team of experts, we eliminate barriers to emulation and enable users of all skill levels to create and interact with software, computer systems, and data from our growing digital heritage.

The System
**********
EaaSI is an emulation curation and access system for the creation, management, and use of emulated computers, software, and digital information. EaaSI provides an intuitive interface through which users may discover available emulation environments and software, import new materials into the system, build new computing environments, and manage access by end-users. The core infrastructure of the system, the Emulation-as-a-Service (EaaS) server software stack, manages emulation-related processes and resources (initializes emulation sessions, provisions compute resources, creates and manages disk images, etc.).

Each EaaSI installation contains a secure OAI-PMH harvester and a data provider to allow multiple instances (nodes) of the system to share metadata of their contents among EaaSI nodes in a trusted network. Nodes within an EaaSI network may use shared computing environments for accessing their digital materials and to create derivative environments from and may also use shared software installation materials to create their own environments.

The Community
*************
The EaaSI Community consists of those people and organizations that use the system and participate in the further development of community practice and professional skills in software preservation and emulation. The Community is an invaluable component of EaaSI, providing the basis for new software features and informing the creation of services that support use and sustainability of the EaaSI program. 

As an affiliated project of the Software Preservation Network (SPN), EaaSI helps advance the organization’s mission and supports the activities of its membership and working groups. EaaSI receives in-kind support through SPN’s communication and outreach apparatus and direct access to the insight and expertise of SPN members.

The Team
********
The members of the EaaSI team are the stewards of the EaaSI program. We utilize our expertise in program management, software development, community cultivation, semantic technology, capacity building, and other areas to create and carry out services in support of the EaaSI system, its users, and the continuous improvement of the EaaSI program’s operations. 

We are responsible for the ongoing development and maintenance of the software in collaboration with OpenSLX, the developers of the EaaS software. In this capacity, EaaSI team members work with OpenSLX to identify requirements, test new features, and manage releases of new software versions.

We support the users of the EaaSI system through cultivation of a growing knowledge base including comprehensive documentation of the system, guidelines and training materials, publications, and other resources. Support requests are addressed to correct bugs in the software or answer questions regarding operation of the system, ensuring a positive user experience of the EaaSI software and services.

To expand the use of EaaSI, the team’s outreach and community cultivation activities raise awareness of the system’s capabilities, the services available to members of the EaaSI Community, and the activities of its membership. Expansion of the EaaSI Community fuels the planning and development of the system by providing information and insights into new use cases and user needs.

We are committed the creation of comprehensive and quality metadata that is of mutual benefit to the EaaSI system and other projects in software preservation, research, and pedagogy. In collaboration with the Wikidata for Digital Preservation project, we aim to facilitate the creation of FAIR, Five-Star Linked Open Data describing items relevant to the program and the domain of computer history.
