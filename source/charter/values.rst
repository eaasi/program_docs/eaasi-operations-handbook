.. Our Values

.. _values:

Our Values
##########

+-----------------+-------------------------------------------------------+
|**Transparent**  |*We are open*                                          |
|                 |                                                       |
|                 |The EaaSI program is committed to open sharing of      |
|                 |information in all aspects of the project, including   |
|                 |details of project technologies, costs, procedures,    |
|                 |and rationale. By engaging in open and honest          |
|                 |communication, we establish trusting relationships,    |
|                 |encourage active participation and feedback, and       |
|                 |contribute our experience and knowledge to our         |
|                 |professional community.                                |
+-----------------+-------------------------------------------------------+
|**Stewards**     |*We think long term*                                   |
|                 |                                                       |
|                 |Our work seeks to expand and improve the capabilities  |
|                 |for the preservation and management of software and    |
|                 |related metadata, aiding like minded organizations in  |
|                 |near and long term. We prioritize the interoperability |
|                 |of our project deliverables to support the exchange of |
|                 |data between a diverse array of systems.               |
+-----------------+-------------------------------------------------------+
|**Accountable**  |*We are trustworthy*                                   |
|                 |                                                       |
|                 |The EaaSI program is engaged with a broad community    |
|                 |of stakeholders, within Yale and beyond, with varying  |
|                 |missions and corresponding requirements related to     |    
|                 |software preservation and access. The EaaSI Team is    |
|                 |committed to a user-driven approach that is responsive |
|                 |to the needs of our community and clearly articulates  |
|                 |the consideration and relevance of user input.         |
+-----------------+-------------------------------------------------------+
|**Inclusive**    |*We value participation*                               |
|                 |                                                       |
|                 |The EaaSI program recognizes the value of a diverse    |
|                 |community of organizations and individuals engaged in  |
|                 |software and digital preservation and seeks to connect |    
|                 |a broad coalition of stakeholders to the work of the   |
|                 |project.                                               |
+-----------------+-------------------------------------------------------+
|**Professional** |*We are reliable, polite, and fair*                    |
|                 |                                                       |
|                 |The EaaSI team values and respects each team member's  |
|                 |work equally. We are committed to maintaining a        |
|                 |positive and equitable work environment. One that      |
|                 |empowers us to work to the best of our abilities and   |
|                 |to balance professional and personal responsibilities. |
+-----------------+-------------------------------------------------------+
|**Collaborative**|*We are part of something bigger*                      |
|                 |                                                       |
|                 |We rely on the involvement and input of partners to    |
|                 |prioritize features, test and provide feedback, and    |
|                 |support the continuing development of emulation        |
|                 |practices for preservation and access. As such, EaaSI  |
|                 |is committed to the explicit recognition of the        |
|                 |contributions made by our partners and will attribute  |
|                 |this work throughout the project and related messaging.|
+-----------------+-------------------------------------------------------+
|**Diverse**      |*We embrace and support difference*                    |
|                 |                                                       |
|                 |Our work promotes diversity in the digital preservation|
|                 |community and the collections of libraries, archives,  |
|                 |and museums to encourage a more equitable              |
|                 |representation of history, opinion, and individual     |
|                 |experience.                                            |
+-----------------+-------------------------------------------------------+
