.. Change Management

.. _change-mgmt:

Change Management
#################

EaaSI’s operational framework is intended to support the success of the team, achievement of the program vision, and contribute to an open, collaborative work environment. In the event that processes are no longer effective or the objectives of the program are no longer relevant, they are open to change, including revision of this Program Charter if necessary. 

Changes to process or program directives may be anonymously proposed at any time via a `change request form <https://docs.google.com/forms/d/1Tj9x6v1oWj2SFyf1v4-HBkDpX7YPRMFu3xJA8-nI1d8/edit?usp=sharing>`_. Team members may also share challenges faced or issues that they wish to address with the team during the quarterly team retrospective (see above).

The Strategic Planning Group will review and respond to proposed changes during their quarterly planning meetings or monthly check-ins. The group will devise an implementation plan for the proposed change and work with the rest of the team to carry out changes. The impact of changes will be reviewed in the quarterly retrospective to determine its success/failure and whether further change is required to address the underlying issue.
