.. Strategic Planning

.. _strategic-planning:

Strategic Planning and Execution
################################

.. image:: ./images/strategic-cycle.png

Quarterly Strategic Planning Meetings
*************************************
The Strategic Planning Group meets quarterly to outline goals, deliverables, and timeline for the upcoming quarter, informed by input from other Program Team members, feedback and data gathered from stakeholders, and other factors impacting the program.

Meeting Agenda
==============
1. Old Business
---------------
To begin the planning portion of the meeting, the group reviews the status of any ongoing work that will continue from the prior quarter. These may simply be deliverables that will be completed during the next quarter but were started in the prior or long-term initiatives that will span numerous planning periods. Each member of the group is expected to provide an update from the work group(s) to which they belong. The Program Manager provides an update on the budget to provide an insight into ongoing expenses and spending behaviors.

  **Outcome:** A consensus understanding of program status and work in progress to consider in planning deliverables and timeline for the upcoming quarter

  **Supporting Documentation:**
  
  * GitLab issue tracking
  * Bug reports
  * Work progress reports from program team members
  * Meeting minutes from previous quarter(s)

2. New Business
---------------
The majority of the strategic planning meeting is focused on discussion and specification of the program’s goals and activities for the upcoming quarter. 

2a. Scheduled Work
^^^^^^^^^^^^^^^^^^
This planning effort starts with a review of known scheduled work, primarily this includes communications/outreach efforts, conference attendance, EaaSI Network activities, and project management deliverables.

  **Outcome:** Identification of upcoming activities to consider in planning deliverables and timeline for the upcoming quarter

  **Supporting Documentation:**
  
  * `Editorial calendar <https://gitlab.com/groups/eaasi/-/boards/1914165>`_
  * `Conference calendar <https://gitlab.com/groups/eaasi/-/boards/895540>`_
  * Software release schedule
  * Maintenance plans
  * GitLab Issues/Milestones

2b. Dev Planning
^^^^^^^^^^^^^^^^
The Strategic Planning Group reviews the backlog of feature proposals to determine which will be included in the next release and will be worked on in the coming quarter. The group revisits proposals that were postponed in previous meetings and determines whether to schedule the proposed feature for development in the following quarter, postpone for reconsideration at a later date, or decline the proposal.

  **Outcome:** Feature proposals approved for development are merged into the main branch of the eaasi_project GitLab repository.

  **Supporting Documentation:**
  
  * Feature proposal `merge requests <https://gitlab.com/eaasi/eaasi/-/merge_requests>`_

2c. Response to retrospective and feedback
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The group discusses comments and suggestions from the Program Team and other stakeholder input that addresses the team’s operations, the goal/scope of the program, and any issues that impact the success of EaaSI. The group identifies strategies to address this feedback through changes in workflows, adjustments to the overall program vision, or other means as needed and will align these changes in the quarterly planning that follows.

  **Outcome:** Directives for changes to program operations or strategy.

  **Supporting Documentation:**
  
  * Retrospective questionnaire responses or change request proposals
  * Discovery information gathered from stakeholder outreach and communications
  * Comments/feedback raised in the `EaaSI User Support Forum <https://forum.eaasi.cloud/c/support-center/6>`_

2d. Long-term strategic planning
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
To complete the new business portion of the meeting, the group addresses issues pertaining to the sustainability of the program and long-term strategy beyond the scope of the current grant period or active projects. This may include selection of grant proposals to pursue and other fundraising opportunities and long-term business planning.

  **Outcome:** Identification of activities pertaining to implementation of long-term strategy.

3. Schedule and capacity management
-----------------------------------
Having determined the work to be done in the following quarter, the strategic planning meeting ends with a review of the full scope of work to align deliverables and other activities in the timeline and manage capacity of individual team members.

  **Outcome:** Schedule of deliverables and activities for upcoming quarter.

  **Supporting Documentation:**
  
  * `Program roadmap <https://gitlab.com/groups/eaasi/-/roadmap>`_
  * `Individual work assignments <https://gitlab.com/groups/eaasi/-/boards/1952703>`_

Quarterly Work Group Planning Meetings
**************************************

The outcome of the strategic planning meeting (immediate objectives, target deliverables, deadlines) are communicated to the program team within a week. If necessary, the Program manager meets with the group to determine how to achieve the objectives and complete the target deliverables for the quarter.

Work Group Check-Ins
********************

Each group meets regularly throughout the quarter to assess progress and adjust their approach to deliverables as needed.

If necessary, the Strategic Planning Group meets monthly to determine whether a substantive change to the quarterly plan is necessary. If they alter plans, these are communicated to the relevant group to be addressed in their next meeting.

.. topic:: Ad Hoc Strategic Planning

    Periodically, the Strategic Planning Group will be required to make strategic decisions outside the typical order of events. In these cases, depending on the magnitude and immediacy of the issue at hand, the group will meet to address the issue or communicate through typical channels (Slack, GitLab, email, etc).
    
    The outcomes of these decisions will be communicated to the rest of the team in the next biweekly meeting or through email.

Quarterly Retrospective
***********************

At the end of the quarter, the Program Team conducts a retrospective meeting to review outcomes of the team’s work and determine necessary changes to work procedures, program scope, and strategy. Prior to the meeting, team members are asked to complete a `quarterly retrospective questionnaire <https://docs.google.com/forms/d/1DsGNpVehFpxMHSi_VfHT1WLG7ZLg07k2rnL7mc8hFw8/edit?usp=sharing>`_ to anonymously share their reflections on the previous quarter and to suggest changes to operations or strategy.
