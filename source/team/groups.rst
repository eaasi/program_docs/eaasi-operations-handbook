.. Work Groups

.. _work-groups:

Work Groups
###########

+--------------------+-----------+--------------------------------------------------------------------+--------------------------------------------------------------------------+
|Name                |Members    |Charge                                                              |Meeting Schedule                                                          |
+====================+===========+====================================================================+==========================================================================+
|**Program Team**    |* Ethan    |* Propose new features for inclusion in the EaaSI system            |* **Meets quarterly** for a retrospective meeting to review achievements, |
|                    |* Euan     |* Identify relevant conferences and paper proposals                 |  discuss challenges, and determine process improvements                  |
|                    |* Klaus    |* Provide input on program directives, activities, and documentation|                                                                          |
|                    |* Jessica  |* Propose new projects for fundraising opportunities                |                                                                          |
|                    |* Oleg     |                                                                    |                                                                          |
|                    |* Seth     |                                                                    |                                                                          |
+--------------------+-----------+--------------------------------------------------------------------+--------------------------------------------------------------------------+
|**Strategic Planning|* Euan     |* Determines the plan of action for EaaSI program according to the  |* **Meets bi-weekly** to set goals, activities, and schedule              |
|Group**             |* Klaus    |  goals and directives of the program team and stakeholders         |  for the program                                                         |
|                    |* Seth     |* Coordinates allocation of staff and financial resources for       |                                                                          |
|                    |           |  program activities                                                |                                                                          |
|                    |           |* Prioritizes features for development based on input from the      |                                                                          |
|                    |           |  program team, stakeholder needs, and program goals                |                                                                          |
+--------------------+-----------+--------------------------------------------------------------------+--------------------------------------------------------------------------+
|**Dev Planning      |* Klaus    |* Identifies development tasks to meet priorities set by the        |* **Meets the first and third Monday** of each month to discuss ongoing   |
|Group**             |* Oleg     |  Strategic Planning Group                                          |  dev work                                                                |
|                    |* Ethan    |* Coordinates testing and release processes                         |                                                                          |
|                    |* Seth     |                                                                    |                                                                          |
+--------------------+-----------+--------------------------------------------------------------------+--------------------------------------------------------------------------+
|**Community Outreach|* Ethan    |* Implements plans for stakeholder outreach and communications      |* **Meets quarterly** to review and plan for scheduled outreach,          |
|Group**             |* Jessica  |* Manages the activities of the members of the EaaSI Network        |  communications, and Network engagement activities                       |
|                    |           |* Coordinates activities of pilot programs and user data gathering  |* **Meets weekly** for check-in with the Program Manager to review        |
|                    |           |                                                                    |  status of activities, answer questions, and reassess timelines as needed|
+--------------------+-----------+--------------------------------------------------------------------+--------------------------------------------------------------------------+
|**Sustainability and|* Euan     |* Develops service models, sustainability plans, and governance     |* **Meets monthly** to discuss topics related to EaaSI sustainability     |
|Governance Group**  |* Jessica  |  frameworks for the future of the EaaSI program                    |  and governance, review deliverables, and determine action items for     |
|                    |* Ethan    |                                                                    |  the following month                                                     |
|                    |* Seth     |                                                                    |                                                                          |
|                    |* Klaus    |                                                                    |                                                                          |
+--------------------+-----------+--------------------------------------------------------------------+--------------------------------------------------------------------------+
|**User and Technical|* Ethan    |* Coordinates responses to support needs of the EaaSI program team  |* **Meets as needed** to review outstanding bug reports, answer questions |
|Support**           |* Oleg     |  and Network members                                               |  related to user support requests, and review timeline for any upcoming  |
|                    |           |                                                                    |  scheduled maintenance                                                   |
+--------------------+-----------+--------------------------------------------------------------------+--------------------------------------------------------------------------+
