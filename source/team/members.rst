.. EaaSI Team

.. _eaasi-team:

EaaSI Team
##########

+-------------------------+---------------------------------------------------------------------------------+
|**Seth Anderson**        |* Oversees and participates in the day-to-day activities of the program team     |
|Co-Principal Investigator|* Manages budget and coordinates spending with University administration and     |
|& Program Manager        |  business office                                                                |
|                         |* Facilitates creation of strategic vision, system requirements, project         | 
|                         |  timelines, and other project management materials                              |
+-------------------------+---------------------------------------------------------------------------------+
|**Euan Cochrane**        |* Guides short and long-term strategic vision and planning of the EaaSI program  |
|Co-Principal Investigator|* Coordinates with University leadership and administration to ensure compliance | 
|                         |  with agency requirements                                                       |
|                         |* Serves as first point-of-contact for funding agencies                          |
|                         |* “Owner” of the Yale EaaSI node                                                 |
+-------------------------+---------------------------------------------------------------------------------+
|**Ethan Gates**          |* Leads creation of guidelines, training materials, and other outreach/user      |
|User Support Lead        |  service documentation and resources                                            |
|                         |* Moderates message boards and user support services (e.g., ticketing system)    |
|                         |* Manages student workforce of configuration and documentation workers           |
|                         |* Plans and facilitates training and outreach events at conferences, partner     |
|                         |  institutions, and prospective users                                            |
+-------------------------+---------------------------------------------------------------------------------+
|**Jessica Meyerson**     |* Cultivates relationships with community members and strategic partners         |
|Community Cultivation &  |* Aids in the specification and implementation of governance procedures and      | 
|Sustainability Lead      |  definition of formal agreements with users                                     |
+-------------------------+---------------------------------------------------------------------------------+
|**Klaus Rechert**        |* Oversees the day-to-day activities of the development team(s)                  |
|Development Manager      |* Contributes to planning and specification of system architecture               |
|                         |  and feature development                                                        |
+-------------------------+---------------------------------------------------------------------------------+
|**Katherine Skinner**    |* Develops cost estimates, stakeholder maps, service models, etc. to aid in      |
|Sustainability &         |  sustainability planning                                                        |
|Governance Consultant    |* Facilitates conversations with YUL leadership and other key stakeholders       |
|                         |  regarding business planning and sustainability                                 |
+-------------------------+---------------------------------------------------------------------------------+
|**Oleg Stobbe**          |* Manages the day-to-day activities of the development team(s)                   |
|Chief Programmer         |* Leads planning and specification of system architecture and feature development|
|                         |* Oversees maintenance activities and responses to bug reports                   |
+-------------------------+---------------------------------------------------------------------------------+
|**Katherine Thornton**   |* Works with developers to ensure continued interoperability of Wikidata, the    |
|Semantic Architect       |  WikiDP Portal, and EaaSI                                                       |
|                         |* Contributes structured data to Wikidata to advance data curation efforts       |
|                         |* Writes shape expression schemas to monitor conformance of items in the software| 
|                         |  and computing domain                                                           |
+-------------------------+---------------------------------------------------------------------------------+
